* Description
  - Assignment 2: Transfer Learning Evaluation Setting

* How to run
  0. Clone this git repository.
     $ git clone https://bitbucket.org/gwangmu/deeplearning_assn02.git
  1. Run 'assn2' to train and test a transfer learning model.
     $ ./assn2
  2. Run 'assn2_scratch' to train and test a scratch model.
     $ ./assn2_scratch

* Troubleshooting
  - Original 'assn2' and 'assn2_scratch' scripts run on
    GPU 0 and GPU 1, respectively. If any model doesn't work 
    properly, modify the solver and model file not to use GPUs.
      (test_solver.prototxt)
        #solver_mode : CPU --> solver_mode : CPU
      (assn2 & assn2_scratch)
        Remove "-gpu x" flags.
  - The 'assn2' and 'assn2_scratch' scripts assume that 'Caffe' binary directory
    has already been registered at the PATH variable. If the scripts cannot
    find the 'caffe' binary, register your 'Caffe' binary directory to the system's
    PATH variable. For example,
      $ export PATH=$PATH:<your_caffe_root_directory>/build/tools
